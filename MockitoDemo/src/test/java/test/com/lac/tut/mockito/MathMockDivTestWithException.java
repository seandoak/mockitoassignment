// Sean Doak A00226304
package test.com.lac.tut.mockito;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;

import com.lac.tut.mockito.Math;

/**
 * Test division method of Math class
 */
public class MathMockDivTestWithException {
	Math mathObj;
	
	@Before
	public void create() throws ArithmeticException {
		//Create Math Object
		mathObj= mock(Math.class); //Create Math Object
		//Configure it to return exception when denominator is zero
		when(mathObj.div(1,0)).thenThrow(new ArithmeticException());
	}
	
	//expect the method throws ArithmeticException
	@Test(expected=ArithmeticException.class) 
	public void test() {
		//call the div and expect to return ArithmeticException
		mathObj.div(1,0); 
	}
}