// Sean Doak A00226304
package test.com.lac.tut.mockito;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import com.lac.tut.mockito.Math;

public class MathMockVerfiyTest {
	Math mathObj;
	
	@Before
	public void create(){
		//Create Math mock Object
		mathObj= mock(Math.class);
		//Add one plus two. The function should return three
		when(mathObj.add(1,2)).thenReturn(3); 
		//when(mathObj.add(5,5)).thenReturn(10); 
	}
	
	@Test
	public void test() {
		//call the add function with 1,2 as arguments
		assertSame(mathObj.add(1,2),3);
		
		//Verify whether add method is tested with arguments 1,2 
		verify(mathObj).add(1,2); 
		
		//Verify whether add method is called only once
		verify(mathObj, times(1)).add(anyInt(),anyInt());
	}
}