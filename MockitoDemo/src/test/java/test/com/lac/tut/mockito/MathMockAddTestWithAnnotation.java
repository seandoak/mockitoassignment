// Sean Doak A00226304
package test.com.lac.tut.mockito;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.lac.tut.mockito.Math;

/**
* Test the add method of Math object with Mockito annotation
*/
public class MathMockAddTestWithAnnotation {
	@Mock
	Math mathObj;
	
	@Before
	/**
	 * Create mock object before you use them
	 */
	public void create() {
		// Initialize this mock objects
		initMocks(this);
		// Configure it to return 3 when arguments passed are 1,2
		when(mathObj.add(1,2)).thenReturn(3); 
	}
	
	@Test
	public void test() {
		//Assert that math object return 3
		assertSame(3, mathObj.add(1,2));
	}
}